from django.db import models

#Clase de Proyecto para la base de datos

class Proyecto(models.Model):
    nombre = models.CharField(max_length=100)
    responsable = models.CharField(max_length=100)
    fecha_creacion = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.nombre

    class Meta:
        app_label = 'app'

#Clase de Actividad para la base de datos
class Actividad(models.Model):
    nombre = models.CharField(max_length=200)
    porcentaje_avance = models.IntegerField(null=True)
    responsable = models.CharField(max_length=200)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    proyecto = models.ForeignKey(Proyecto, 
    related_name= 'actividades', 
    on_delete= models.CASCADE,
    null=True)

    def __str__(self):
        return self.nombre

    class Meta:
        app_label = 'app'
