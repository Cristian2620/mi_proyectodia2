from django.urls import path
from . import views

urlpatterns =[
    path('',views.index, name='index'),
    path('proyectos/',views.proyectos, name='proyectos'),
    path('proyectos/<int:id>/',views.proyecto, name='proyecto'),
    path('proyectos/crear/',views.form_crear_proyecto, name='form_crear_proyecto'),
    path('proyectos/crear_post/',views.post_crear_proyecto, name='post_crear_proyecto'),
    path('actividades/',views.actividades, name='actividades')
]