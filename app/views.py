from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django.urls import reverse
from .models import Proyecto, Actividad
def index(request):
    return HttpResponse('Inicio')

def proyectos(request):
    # Trae todos los proyectos de la tabla de la BD
    proyectos_lista = Proyecto.objects.all()

    #Asigna proyectos a la lista
    contexto = {
        'proyectos' : proyectos_lista
       
    }
    return render(request, 'app/proyectos.html', contexto)


def actividades(request):
    # Trae todos las actividades de la tabla de la BD
    actividades_lista = Actividad.objects.all()
    print(actividades_lista)
    contexto = {
        'actividades' : actividades_lista
    }
    return render(request, 'app/actividades.html', contexto)

def form_crear_proyecto(request):
    contexto = {
    
    }
    return render(request, 'app/form_crear_proyecto.html', contexto)

def post_crear_proyecto(request):
    #obtiene los datos del formulario
    nombre_param = request.POST['nombre']
    responsable_param = request.POST['responsable']

    #Crea el proyecto en la base de datos
    p = Proyecto(nombre=nombre_param, responsable=responsable_param)
    p.save()
    return HttpResponseRedirect(reverse('proyectos'))

def proyecto(request, id ):
    p = Proyecto.objects.get(pk=id)

    contexto = {
        'proyecto' : p
    }
    print("llega")
    return render(request, 'app/proyecto.html', contexto)

   

